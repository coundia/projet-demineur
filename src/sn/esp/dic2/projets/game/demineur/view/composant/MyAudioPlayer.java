package sn.esp.dic2.projets.game.demineur.view.composant;

import java.io.*;
import javazoom.jl.player.*;

public class MyAudioPlayer extends Thread {
	
    private String fileLocation;
    private static String sonDecouvre="./music/decouvrir.mp3",sonMarque="./music/marquer.mp3",
    		sonGagner="./music/gagner.mp3",sonExplosionMine="./music/miner.mp3";
    private boolean loop;
    private Player prehravac;
    public static boolean mute=false;

    public MyAudioPlayer(String fileLocation, boolean loop) {
        this.fileLocation = fileLocation;
        this.loop = loop;
    }

    public void run() {

        try {
            do {
                FileInputStream buff = new FileInputStream(fileLocation);
                prehravac = new Player(buff);
                prehravac.play();
            } while (loop);
        } catch (Exception ioe) {
            // TODO error handling
        }
    }

    public void close(){
        loop = false;
        prehravac.close();
        this.interrupt();
    }
	public static void main(String[] args){
		MyAudioPlayer thePlayer = new MyAudioPlayer("./music/disiz.mp3", true);
	    thePlayer.start();
		
	}
	public static void  sonDecouvrir(){
		if (mute==false){
			MyAudioPlayer thePlayer = new MyAudioPlayer(sonDecouvre, false);
		    thePlayer.start();
		}
		
	}
	public static void  sonMarque(){
		if (mute==false){
			MyAudioPlayer thePlayer = new MyAudioPlayer(sonMarque, false);
		    thePlayer.start();
		}
	}
	public static void  sonMine(){
		if (mute==false){
			MyAudioPlayer thePlayer = new MyAudioPlayer(sonExplosionMine, false);
		    thePlayer.start();
		}
		
	}
	public static void  sonGagner(){
		if (mute==false){
			MyAudioPlayer thePlayer = new MyAudioPlayer(sonGagner, false);
		    thePlayer.start();
		}
	}
}