package sn.esp.dic2.projets.game.demineur.view.composant;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;

public class MenuButton extends JButton {
	public static final  int menu=0,option=1,action=2,text=3;
	private int type;
	private boolean selected;
	public MenuButton(String text,int type){
		super(text);
		this.type=type;
		initialise();
		 
	}//MenuButton
	//initialise le button suivant son type
	public void initialise(){
		switch (type) {
		case MenuButton.menu:
			this.setPreferredSize(new Dimension(75,75));
			this.setBackground(ThemeDemineur.getCouleur());
			this.setForeground(ThemeDemineur.getCouleurEcriture());
			this.setFont(ThemeDemineur.getFontText2());
			break;
		case MenuButton.option:
			this.setPreferredSize(new Dimension(150,50));
			this.setBackground(ThemeDemineur.getCouleur());
			this.setForeground(ThemeDemineur.getCouleurEcriture());
			this.setFont(ThemeDemineur.getFontText1());
			break;
		case MenuButton.action:
			this.setPreferredSize(new Dimension(125,10));
			this.setBackground(ThemeDemineur.getCouleur());
			this.setForeground(ThemeDemineur.getCouleurEcriture());
			this.setFont(ThemeDemineur.getFontText1());
			break;
		case MenuButton.text:
			this.setBackground(ThemeDemineur.getCouleur());
			this.setForeground(ThemeDemineur.getCouleurEcriture());
			this.setFont(ThemeDemineur.getFontText2());
			break;
		default:
			break;
		}
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
		if (isSelected()){
			this.setBackground(ThemeDemineur.getCouleurEcriture());
			this.setForeground(ThemeDemineur.getCouleur());
		}else{
			this.setBackground(ThemeDemineur.getCouleur());
			this.setForeground(ThemeDemineur.getCouleurEcriture());
		}
	}

}
