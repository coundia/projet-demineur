package sn.esp.dic2.projets.game.demineur.view.composant;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

public class DemineuFileChooser extends JFileChooser {
	public DemineuFileChooser() {
		
	}

	 
	
	public String ouvrirFile(){
		String filename;
		int retour=this.showOpenDialog(null);
		if (retour == JFileChooser.APPROVE_OPTION) {
            File file = this.getSelectedFile();
            filename=file.getAbsolutePath();
            System.out.println(filename);
            
        } else {
            filename=null;
        }
		return filename;
	}
	public String enregistrerFile(){
		String filename;
		int retour=this.showSaveDialog(null);
		if (retour == JFileChooser.APPROVE_OPTION){
            File file = this.getSelectedFile();
            filename=file.getAbsolutePath();
            System.out.println(filename);
            
        } else {
            filename=null;
        }
		return filename;
	}

}
