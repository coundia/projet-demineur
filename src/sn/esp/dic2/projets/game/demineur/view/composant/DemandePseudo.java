package sn.esp.dic2.projets.game.demineur.view.composant;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import java.awt.FlowLayout;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Dialog.ModalityType;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DemandePseudo extends JDialog{
	private JTextField txtAnon;
	 
	 

	/**
	 * Create the application.
	 */
	public DemandePseudo() {
		initialize();
	}

	/**
	 * Initialisee lecontenu du frame.
	 */
	private void initialize() {
		
		getContentPane().setFont(new Font("Microsoft YaHei", Font.PLAIN, 13));
		setModalityType(ModalityType.DOCUMENT_MODAL);
		setTitle("Changer le nom du joueur");
		
		this.setBounds(100, 100, 450, 300);
		this.getContentPane().setLayout(null);
		
		JLabel lbPseudo = new JLabel("Votre pseudo");
		lbPseudo.setBounds(139, 8, 64, 14);
		this.getContentPane().add(lbPseudo);
		
		txtAnon = new JTextField();
		txtAnon.setBounds(208, 5, 86, 20);
		txtAnon.setText("anon");
		this.getContentPane().add(txtAnon);
		txtAnon.setColumns(10);
		
		JButton btnNewButton = new JButton("enregistrer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtAnon.getText().equalsIgnoreCase("")){
					JOptionPane.showMessageDialog(DemandePseudo.this, "Le pseudo ne doit pas etre nul") ;
				}else{
					DemandePseudo.this.setVisible(false);
				}
				
			}
		});
		btnNewButton.setBounds(139, 215, 135, 23);
		this.getContentPane().add(btnNewButton);
		
		JButton btnAnnuler = new JButton("annuler");
		btnAnnuler.setBounds(284, 215, 140, 23);
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DemandePseudo.this.dispose();
			}
		});
		this.getContentPane().add(btnAnnuler);
	}
	public String getDonnees(){
		  
		  return this.txtAnon.getText();
	}
}
