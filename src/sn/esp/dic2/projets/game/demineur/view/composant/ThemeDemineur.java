package sn.esp.dic2.projets.game.demineur.view.composant;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;

public class ThemeDemineur {
	
	private static String[] dossiersThemes={"./image/theme1/","./image/theme2/","./image/theme3/","./image/theme4/","./image/theme5/","./image/theme6/"};
	private static String   racine=dossiersThemes[0];
	private static ImageIcon[] CaseImage;
	private static ImageIcon caseViergeImage=new ImageIcon(racine+ "imageCaseVierge.jpg");
	private static ImageIcon mineImage=new ImageIcon(racine+"imageCaseMine.jpg");;
	private static ImageIcon minActifImage=new ImageIcon(racine+"imageCaseMineActif.jpg");
	private static ImageIcon caseSuspecteImage=new ImageIcon(racine+"imageCaseSuspecte.jpg");
	private static ImageIcon caseMarqueImage=new ImageIcon(racine+"imageCaseMarque.jpg");
	private static ImageIcon backgroundImage=new ImageIcon(racine+"background.jpg");
	private static ImageIcon iconParametre=new ImageIcon(racine+"iconparametre.png");
	private static ImageIcon iconGame=new ImageIcon(racine+"iconGame.png");
	private static ImageIcon iconMenu=new ImageIcon(racine+"iconMenu.png");
	private static ImageIcon iconSon=new ImageIcon(racine+"son.png");
	private static ImageIcon iconMute=new ImageIcon(racine+"mute.png");
	private static ImageIcon emoticonJouer=new ImageIcon(racine+"jouer.gif");
	private static ImageIcon emoticonGagner=new ImageIcon(racine+"gagner.gif");
	private static ImageIcon emoticonPerdre=new ImageIcon(racine+"explosion.gif");
	private static ImageIcon emoticonPause=new ImageIcon(racine+"pause.jpg");
	private static Font fontnumber=new Font("Imprint MT Shadow", 1, 36);
	private static Font fontText1=new Font("Verdana", 1, 16);
	private static Font fontText2=new Font("Verdana", 1, 6);
	private static Font fontText3=new Font("Verdana", 1, 25);
	private static Color[] couleurs={new Color(255, 255, 255),//theme1
									new Color(177, 90, 78),//theme2
									new Color(150, 150, 150),//theme3
									new Color(255, 255, 255),//theme4
									new Color(255, 192, 0),//theme5
									new Color(255, 192, 0),//theme6
									};
	private static Color[] couleurEcritures={new Color(0, 0, 255),//theme1
											new Color(255, 91, 7),//theme2
											new Color(255, 91, 7),//theme3
											new Color(22, 67, 45),//theme4
											new Color(0, 0, 0),//theme5
											new Color(0, 0, 0),//theme6
		};
	private static Color couleur=couleurs[0];
	private static Color couleurEcriture=couleurEcritures[0];
	public static ImageIcon getCaseImage(int i){
		if(CaseImage==null){
			initialiseCaseImage();
		}
		return CaseImage[i];
	}
	
	/**
	 * reche=arge de toutes les images suivant le theme
	 */
	public static  void initialiseCaseImage(){
		
		CaseImage=new ImageIcon[9];
		for (int i = 0; i < CaseImage.length; i++) {
			CaseImage[i]=new ImageIcon(racine+"imageCase"+i+".jpg"); 
			 
		}
		 caseViergeImage=new ImageIcon(racine+ "imageCaseVierge.jpg");
		 mineImage=new ImageIcon(racine+"imageCaseMine.jpg");;
		 minActifImage=new ImageIcon(racine+"imageCaseMineActif.jpg");
		 caseSuspecteImage=new ImageIcon(racine+"imageCaseSuspecte.jpg");
		 caseMarqueImage=new ImageIcon(racine+"imageCaseMarque.jpg");
		 backgroundImage=new ImageIcon(racine+"background.jpg");
		iconParametre=new ImageIcon(racine+"iconparametre.png");
		 iconGame=new ImageIcon(racine+"iconGame.png");
		 iconMenu=new ImageIcon(racine+"iconMenu.png");
		iconSon=new ImageIcon(racine+"son.png");
		iconMute=new ImageIcon(racine+"mute.png");
		emoticonJouer=new ImageIcon(racine+"jouer.gif");
		emoticonGagner=new ImageIcon(racine+"gagner.gif");
		emoticonPerdre=new ImageIcon(racine+"explosion.gif");
		emoticonPause=new ImageIcon(racine+"pause.jpg");
	}//initialiseCaseImage
	public static ImageIcon getCaseViergeImage() {
		return caseViergeImage;
	}
	public static void setCaseViergeImage(ImageIcon caseViergeImage) {
		ThemeDemineur.caseViergeImage = caseViergeImage;
	}
	public static ImageIcon getMineImage() {
		return mineImage;
	}
	public static void setMineImage(ImageIcon mineImage) {
		ThemeDemineur.mineImage = mineImage;
	}
	public static ImageIcon getMinActifImage() {
		return minActifImage;
	}
	public static void setMinActifImage(ImageIcon minActifImage) {
		ThemeDemineur.minActifImage = minActifImage;
	}
	public static Font getFontnumber() {
		return fontnumber;
	}
	public static void setFontnumber(Font fontnumber) {
		ThemeDemineur.fontnumber = fontnumber;
	}
	public static Font getFontText1() {
		return fontText1;
	}
	public static void setFontText1(Font fontText1) {
		ThemeDemineur.fontText1 = fontText1;
	}
	public static Font getFontText2() {
		return fontText2;
	}
	public static void setFontText2(Font fontText2) {
		ThemeDemineur.fontText2 = fontText2;
	}
	public static ImageIcon getCaseSuspecteImage() {
		return caseSuspecteImage;
	}
	public static void setCaseSuspecteImage(ImageIcon caseSuspecteImage) {
		ThemeDemineur.caseSuspecteImage = caseSuspecteImage;
	}
	public static ImageIcon getCaseMarqueImage() {
		return caseMarqueImage;
	}
	public static void setCaseMarqueImage(ImageIcon caseMarqueImage) {
		ThemeDemineur.caseMarqueImage = caseMarqueImage;
	}
	public static Color getCouleur() {
		return couleur;
	}
	public static void setCouleur(Color couleur) {
		ThemeDemineur.couleur = couleur;
	}
	public static Color getCouleurEcriture() {
		return couleurEcriture;
	}
	public static void setCouleurEcriture(Color couleurEcriture) {
		ThemeDemineur.couleurEcriture = couleurEcriture;
	}
	public static ImageIcon getBackgroundImage() {
		return backgroundImage;
	}
	public static void setBackgroundImage(ImageIcon backgroundImage) {
		ThemeDemineur.backgroundImage = backgroundImage;
	}
	
	public static ImageIcon getIconParametre() {
		return iconParametre;
	}

	public static void setIconParametre(ImageIcon iconParametre) {
		ThemeDemineur.iconParametre = iconParametre;
	}

	public static ImageIcon getIconGame() {
		return iconGame;
	}

	public static void setIconGame(ImageIcon iconGame) {
		ThemeDemineur.iconGame = iconGame;
	}

	public static ImageIcon getIconMenu() {
		return iconMenu;
	}

	public static void setIconMenu(ImageIcon iconMenu) {
		ThemeDemineur.iconMenu = iconMenu;
	}

	/**
	 * donne le noombre de theme
	 * @return
	 */
	public static int getNbTheme(){
		return dossiersThemes.length;
	}
	/**
	 * image illustrant le theme a la position i
	 * @param i
	 * @return
	 */
	public static ImageIcon getThemeImage(int i){
		return new ImageIcon(dossiersThemes[i]+"background.jpg");
	}
	/**
	 * change le theme
	 */
	public static void setTheme(int i){
		racine=dossiersThemes[i];
		couleur=couleurs[i];
		couleurEcriture=couleurEcritures[i];
		initialiseCaseImage();
	}

	public static ImageIcon getIconSon() {
		return iconSon;
	}

	public static void setIconSon(ImageIcon iconSon) {
		ThemeDemineur.iconSon = iconSon;
	}

	public static ImageIcon getIconMute() {
		return iconMute;
	}

	public static void setIconMute(ImageIcon iconMute) {
		ThemeDemineur.iconMute = iconMute;
	}

	public static ImageIcon getEmoticonJouer() {
		return emoticonJouer;
	}

	public static void setEmoticonJouer(ImageIcon emoticonJouer) {
		ThemeDemineur.emoticonJouer = emoticonJouer;
	}

	public static ImageIcon getEmoticonGagner() {
		return emoticonGagner;
	}

	public static void setEmoticonGagner(ImageIcon emoticonGagner) {
		ThemeDemineur.emoticonGagner = emoticonGagner;
	}

	public static ImageIcon getEmoticonPerdre() {
		return emoticonPerdre;
	}

	public static void setEmoticonPerdre(ImageIcon emoticonPerdre) {
		ThemeDemineur.emoticonPerdre = emoticonPerdre;
	}

	public static ImageIcon getEmoticonPause() {
		return emoticonPause;
	}

	public static void setEmoticonPause(ImageIcon emoticonPause) {
		ThemeDemineur.emoticonPause = emoticonPause;
	}

	public static Font getFontText3() {
		return fontText3;
	}

	public static void setFontText3(Font fontText3) {
		ThemeDemineur.fontText3 = fontText3;
	}
	
}
