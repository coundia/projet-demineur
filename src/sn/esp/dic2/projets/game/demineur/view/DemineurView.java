package sn.esp.dic2.projets.game.demineur.view;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.*;

import sn.esp.dic2.projets.game.demineur.controller.ControllerChangeEvent;
import sn.esp.dic2.projets.game.demineur.controller.ControllerChangeListerner;
import sn.esp.dic2.projets.game.demineur.controller.ModelChangeEvent;
import sn.esp.dic2.projets.game.demineur.controller.ModelChangeListerner;
import sn.esp.dic2.projets.game.demineur.view.composant.MyAudioPlayer;
import sn.esp.dic2.projets.game.demineur.view.composant.ThemeDemineur;
public class DemineurView extends JPanel implements ModelChangeListerner{
	private int largeur;
	private int longueur;
	ArrayList<ControllerChangeListerner> listeners;
	CaseView[][] caseviews;
	public DemineurView() { 
	}
	public DemineurView(int nbLigne,int nbColonne){
		super();
		this.setLayout(new GridLayout(nbLigne,nbColonne,5,5));
		this.caseviews = new CaseView[nbLigne][nbColonne];
		for (int i = 0; i < nbLigne; i++) {
			for (int j = 0; j < nbColonne; j++) {
				caseviews[i][j]=new CaseView(this,i, j);
				this.add(caseviews[i][j]);
//				caseviews[i][j].setIcon(ThemeDemineur.getCaseViergeImage());
				
			}//for
			
		}//for
		this.listeners=new ArrayList<ControllerChangeListerner>();
		setPreferredSize(new Dimension(600, 600));
		this.setOpaque(false);
	}
	public void addControllerChangeListerner(ControllerChangeListerner l){
		this.listeners.add(l);
	}
	public void removeControllerChangeListerner(ControllerChangeListerner l){
		this.listeners.remove(l);
	}
	public void fireControllerChangeLister(ControllerChangeEvent evt){
		for (Iterator<ControllerChangeListerner> iterator = this.listeners.iterator(); iterator.hasNext();) {
			ControllerChangeListerner controller = (ControllerChangeListerner) iterator.next();
			controller.controllerChanged(evt);
			
		}
	}
	@Override
	public void modelChanged(ModelChangeEvent evt) {
		if(evt.getColonne()==-1&&evt.getLigne()==-1)return;//si l evenement ne concerne pas une case
		System.out.println("modechange"+evt); 
		/*
		 * si il y a une mine dans la case a redessiner
		 * soit l etat de la partie est perdu donc  on vient de clicker sur un mine
		 */
		if(evt.isMine()){		 
			 
			if (evt.getEtatPartie()==ModelChangeEvent.PERDU){
				caseviews[evt.getLigne()][evt.getColonne()].setIcon(ThemeDemineur.getMinActifImage());
				if(!evt.isModeSilent())MyAudioPlayer.sonMine();
				JOptionPane.showMessageDialog(null, "vous avez perdu");
			}else{
				caseviews[evt.getLigne()][evt.getColonne()].setIcon(ThemeDemineur.getMineImage());
				 
			}
			
		}else
			if(evt.getEtatPartie()==ModelChangeEvent.GAGNER){
				System.out.println("gagner");
				caseviews[evt.getLigne()][evt.getColonne()].setIcon(ThemeDemineur.getCaseImage(evt.getPoids()));
				if(!evt.isModeSilent())MyAudioPlayer.sonGagner();
				JOptionPane.showMessageDialog(null, "vous avez gagn�");
				
		}else
			if(evt.getEtatCase()==ModelChangeEvent.REVELED  ){
				caseviews[evt.getLigne()][evt.getColonne()].setIcon(ThemeDemineur.getCaseImage(evt.getPoids()));
				if(!evt.isModeSilent())MyAudioPlayer.sonDecouvrir();

			}
		else
		if(evt.getEtatCase()==ModelChangeEvent.MARKED){
			if(!evt.isModeSilent())MyAudioPlayer.sonMarque();
			caseviews[evt.getLigne()][evt.getColonne()].setIcon(ThemeDemineur.getCaseMarqueImage());
		}
		else
			if(evt.getEtatCase()==ModelChangeEvent.SUSPECTED){
				caseviews[evt.getLigne()][evt.getColonne()].setIcon(ThemeDemineur.getCaseSuspecteImage());
				if(!evt.isModeSilent())MyAudioPlayer.sonMarque();
			}
			else
				{
				//System.out.println("retour etat normal");
				caseviews[evt.getLigne()][evt.getColonne()].setEnabled(true);
				caseviews[evt.getLigne()][evt.getColonne()].setIcon(ThemeDemineur.getCaseViergeImage());
				}
		
	}
}
