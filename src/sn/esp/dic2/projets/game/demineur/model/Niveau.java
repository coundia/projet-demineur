package sn.esp.dic2.projets.game.demineur.model;

/**
 * Cette classe permet de représenter un niveau du jeu. Avec les possibilités suivantes :
 * <ul>
 * 	<li>Facile</li>
 * 	<li>Moyen</li>
 * 	<li>Difficile</li>
 * 	<li>Personnalisé</li>
 * Chaque niveau est caractérisé par un nombre de cases et de mines.
 * </ul>
 * @author Kenz
 *
 */

public class Niveau {
	;
	public static final int facile = 0, moyen = 1, difficile = 2,
			personnalise = 3;
	private int type;
	private int nbLigne;
	private int nbColonne;
	private int nbMine;

	/**
	 * Constructeur par défaut. Obligatoire pour la sérialisation XML
	 */
	public Niveau() {

	}

	public Niveau(int type) {
		this.type = type;
		switch (type) {
		case Niveau.facile:
			this.nbLigne = 9;
			this.nbColonne = 9;
			//this.nbMine = 1;
			this.nbMine = 10;
			break;
		case Niveau.moyen:
			this.nbLigne = 15;
			this.nbColonne = 10;
			this.nbMine = 25;
			break;
		case Niveau.difficile:
			this.nbLigne = 30;
			this.nbColonne = 20;
			this.nbMine = 100;

			break;
		default:
			this.nbLigne = 9;
			this.nbColonne = 9;
			this.nbMine = 10;
			break;
		}

	}

	public Niveau(int nbLigne, int nbColonne, int nbMine) {
		super();
		this.nbLigne = nbLigne;
		this.nbColonne = nbColonne;
		this.nbMine = nbMine;
	}

	public int getNbLigne() {
		return nbLigne;
	}

	public void setNbLigne(int nbLigne) {
		this.nbLigne = nbLigne;
	}

	public int getNbColonne() {
		return nbColonne;
	}

	public void setNbColonne(int nbColonne) {
		this.nbColonne = nbColonne;
	}

	public int getNbMine() {
		return nbMine;
	}

	public void setNbMine(int nbMine) {
		this.nbMine = nbMine;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
