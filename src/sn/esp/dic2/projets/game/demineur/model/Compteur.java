package sn.esp.dic2.projets.game.demineur.model;

import java.awt.event.ActionListener;

public class Compteur {
	private int minutes = 0, secondes = 0, heures = 0;
	int delay = 1000;

	public Compteur() {

	}

	public void debuter() {

	}

	public void stop() {

	}

	public int getMinutes() {
		return minutes;
	}

	public void minutesInc() {
		this.minutes++;
	}

	public int getSecondes() {
		return secondes;
	}

	public void secondesInc() {
		this.secondes++;
	}

	public int getHeures() {
		return heures;
	}

	public void heuresInc() {
		this.heures++;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public void setSecondes(int secondes) {
		this.secondes = secondes;
	}

	public void setHeures(int heures) {
		this.heures = heures;
	}

	public int getTimeEnSeconde() {
		return heures * 3600 + secondes * 60 + secondes;
	}
}
