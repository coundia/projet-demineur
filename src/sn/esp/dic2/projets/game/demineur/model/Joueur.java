package sn.esp.dic2.projets.game.demineur.model;

/**
 * Cette classe repr�sente un joueur.
 * 
 * @author Kenz
 *
 */
public class Joueur {

	private String pseudo;

	/**
	 * Constructeur par d�faut. Obligatoire pour la s�rialisation XML.
	 */
	public Joueur(String pseudo) {
		super();

		this.pseudo = pseudo;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

}
