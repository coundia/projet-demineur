package sn.esp.dic2.projets.game.demineur.model;

import java.io.Serializable;
import java.util.Arrays;

import sn.esp.dic2.projets.game.demineur.model.Case.TypeEtat;


/**
 * Cette classe repr�sente la grille du jeu.
 * 
 * @author Kenz
 *
 */
public class Grille implements Serializable, Cloneable {
	private Case[][] cases;
	private int nbLigne, nbColonne;
	private DemineurModel model;

	/**
	 * Constructeur par d�faut. Obligatoire pour la s�rialisation XML
	 */
	public Grille() {

	}

	/**
	 * Constructeur d'une grille.<br/>
	 * Nombre de lignes de la grille.
	 * 
	 * @param nbLigne
	 *            Nombre de colonnes de la grille.
	 * @param nbColonne
	 *            R�f�rence de l'instance du Mod�le qui contient la grille.
	 * @param model
	 */
	public Grille(int nbLigne, int nbColonne, DemineurModel model) {
		// on instancie la grille
		cases = new Case[nbLigne][nbColonne];
		// on instanttie chaque case de la grille
		int k = 0;
		for (int i = 0; i < nbLigne; i++) {
			for (int j = 0; j < nbColonne; j++) {
				System.out.println("borne cons" + i + "et" + j);
				cases[i][j] = new Case(i, j, false, this);
			}// for

		}// for

		// on melange la grille pour dispers� les mine
		this.nbLigne = nbLigne;
		this.nbColonne = nbColonne;
		this.model = model;
		melangerGrille();
	}

	/**
	 * Permet de r�cup�rer une case de la grille.
	 * 
	 * @param i
	 * @param j
	 * @return
	 */
	public Case getCase(int i, int j) {

		if (i >= 0 && i < this.nbLigne && j >= 0 && j < this.nbColonne) {
			return cases[i][j];
		} else
			return null;
	}

	public void setCase(int i, int j, Case c) {
		// System.out.println("setcase dans la case " + i + "," + j + "="
		// + this.cases[i][j]);
		this.cases[i][j] = c;
		// System.out.println("setcase ensuite dans la case " + i + "," + j +
		// "="
		// + c);
		// System.out.println("ensuite dans la case " + i + "," + j + "="
		// + this.cases[i][j]);

	}

	/**
	 * 
	 */
	private void melangerGrille() {
		// on parcours le tableau de case
		// pour chak case on le deplace a une position au hasard

		int i, j, k = 1;
		int maxIndexLigne = this.nbLigne - 1;
		int maxIndexColonne = this.nbColonne - 1;
		System.out.println(this.nbLigne + "et" + this.nbColonne);
		while (k <= this.getModel().getNiveau().getNbMine()) {
			// i = (int )((max-min)*Math.random()+min)
			i = (int) ((maxIndexLigne - 0) * Math.random() + 0);
			j = (int) ((maxIndexColonne - 0) * Math.random() + 0);
			System.out.println("ramdom" + i + "et" + j);
			if (!cases[i][j].isMine()) {
				cases[i][j].setMine(true);
				k++;
			}

		}
	}

	/**
	 * Permet de d�couvrir toutes les cases vierges min�es de la grille. <br/>
	 * <em>Lorsque la partie est perdue.</em>
	 */
	public void decouvrirToutesLesCasesMinees() {

		for (int i = 0; i < this.nbLigne; i++) {
			for (int j = 0; j < this.nbColonne; j++) {
				// System.out.println(i+"et"+j+"l"+this.nbLigne);
				if (cases[i][j].getEtat() == TypeEtat.vierge
						&& cases[i][j].isMine()) {
					cases[i][j].decouvrir();
				}
			}
		}

	}

	public DemineurModel getModel() {
		return model;
	}

	public void setModel(DemineurModel model) {
		this.model = model;
	}

	public Case[][] getCases() {
		return cases;
	}

	public void setCases(Case[][] cases) {
		this.cases = cases;
	}

	public int getNbLigne() {
		return nbLigne;
	}

	public void setNbLigne(int nbLigne) {
		this.nbLigne = nbLigne;
	}

	public int getNbColonne() {
		return nbColonne;
	}

	public void setNbColonne(int nbColonne) {
		this.nbColonne = nbColonne;
	}

	/**
	 * Pour cloner la grille.
	 */
	public Grille clone() {
		Grille grilClone = null;
		try {
			grilClone = (Grille) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		grilClone.cases = new Case[grilClone.nbLigne][grilClone.nbColonne];

		for (int i = 0; i < grilClone.nbLigne; i++) {
			for (int j = 0; j < grilClone.nbColonne; j++) {
				grilClone.setCase(i, j, this.cases[i][j].clone());
				// l instance grille dans la case doit etre
				// remplacer dans par l instance d un nouveau grille
				System.out.println(i + "borne" + j);
				grilClone.getCase(i, j).setGrille(grilClone);
			}// for j
		}// for i
		return grilClone;
	}// clone
}
