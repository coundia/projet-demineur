package sn.esp.dic2.projets.game.demineur.controller;

import javax.swing.event.ChangeEvent;

public class ModelChangeEvent extends ChangeEvent {
	public final static int VIERGE=-1,REVELED=0,MARKED=1,SUSPECTED=2,ENCOURS=5,GAGNER=3,PERDU=4,PAUSE=6;
	private int ligne;
	private int colonne;
	private int etatPartie;
	private int etatCase;
	private boolean mine=false;
	private int poids;
	private int score;
	private int mineRestant;
	private boolean modeSilent=false;

	

	public ModelChangeEvent(Object arg0, int ligne, int colonne) {
		super(arg0);
		this.ligne = ligne;
		this.colonne = colonne;
		this.etatCase=VIERGE;
		this.etatPartie=ENCOURS;
	}

	public int getLigne() {
		return ligne;
	}

	public void setLigne(int ligne) {
		this.ligne = ligne;
	}

	public int getColonne() {
		return colonne;
	}

	public void setColonne(int colonne) {
		this.colonne = colonne;
	}

 
	public int getPoids() {
		return poids;
	}

	public void setPoids(int poids) {
		this.poids = poids;
	}





	public int getEtatPartie() {
		return etatPartie;
	}

	public void setEtatPartie(int etatPartie) {
		this.etatPartie = etatPartie;
	}

	public int getEtatCase() {
		return etatCase;
	}

	public void setEtatCase(int etatCase) {
		this.etatCase = etatCase;
	}

	public boolean isMine() {
		return mine;
	}

	public void setMine(boolean mine) {
		this.mine = mine;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getMineRestant() {
		return mineRestant;
	}

	public void setMineRestant(int mineRestant) {
		this.mineRestant = mineRestant;
	}

	@Override
	public String toString() {
		return "ModelChangeEvent [ligne=" + ligne + ", colonne=" + colonne
				+ ", etatPartie=" + etatPartie + ", etatCase=" + etatCase
				+ ", mine=" + mine + ", poids=" + poids + "]";
	}

	public boolean isModeSilent() {
		return modeSilent;
	}

	public void setModeSilent(boolean modeSilent) {
		this.modeSilent = modeSilent;
	}
	

	


}
