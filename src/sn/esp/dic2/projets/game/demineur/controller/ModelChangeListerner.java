package sn.esp.dic2.projets.game.demineur.controller;

import java.util.EventListener;


public interface ModelChangeListerner extends EventListener {

	public void modelChanged(ModelChangeEvent evt);

}
