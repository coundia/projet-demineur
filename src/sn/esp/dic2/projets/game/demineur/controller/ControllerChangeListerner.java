package sn.esp.dic2.projets.game.demineur.controller;

import java.util.EventListener;
public interface ControllerChangeListerner extends EventListener {

	public void controllerChanged(ControllerChangeEvent evt);

}
